﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace GitLabApi
{
	class Program
	{

		private static string ProjectNamePrefix = "GitLabApi";

		private static string CommonProjectName = "Common";
		
		//firstCommitId = "193f460a124b4f1e63c217ea172004516e1b150c";
		static void Main(string[] args)
		{
			var gitLab = new GitLab(new HttpClient());
			
			var firstCommitId = gitLab.GetMasterLastPushedCommitId();

			// after some commits the following code should be called from jenkins.
			var lastMasterCommitsIds = gitLab.GetLastPushedCommitsIds(firstCommitId);

			var commitedFiles = gitLab.GetCommitedFiles(lastMasterCommitsIds).Distinct();

			var projectsNames = GetProjectNamesByFilesPaths(commitedFiles);

		}

		private static string[] GetProjectNamesByFilesPaths(IEnumerable<string> commitedFiles)
		{
			var projectNames = new List<string>();
			foreach (var commitedFileFullPath in commitedFiles)
				GetProjectNameOrDefault(commitedFileFullPath, projectNames);

			RemoveNullProjectNames(projectNames);
			return projectNames.Distinct().ToArray();
		}

		private static void GetProjectNameOrDefault(string commitedFileFullPath, List<string> projectNames)
		{
			if (!IsACommonProjectFile(commitedFileFullPath))
				projectNames.Add(GetProjectName(commitedFileFullPath));
		}

		private static void RemoveNullProjectNames(List<string> projectNames)
		{
			projectNames.RemoveAll(_ => _ == null);
		}

		private static string GetProjectName(string fileFullPath)
		{
			string onlyProjectName = null;
			var projectNameStartAtIndex = fileFullPath.LastIndexOf(ProjectNamePrefix);
			if (IsFileInsideProject(projectNameStartAtIndex))
			{
				var projectNameToEndOfPath = fileFullPath.Substring(projectNameStartAtIndex);
				onlyProjectName = projectNameToEndOfPath.Substring(0, GetEndOfProjectNameIndex(projectNameToEndOfPath));
			}
			return onlyProjectName;
		}

		private static bool IsFileInsideProject(int projectNameStartAtIndex)
		{
			return projectNameStartAtIndex != -1;
		}

		private static int GetEndOfProjectNameIndex(string projectNameToEndOfPath)
		{
			var indexOfProjectInDirectory = projectNameToEndOfPath.IndexOf("/");
			if (indexOfProjectInDirectory == -1)
				return projectNameToEndOfPath.IndexOf(".");
			return indexOfProjectInDirectory;
		}

		private static bool IsACommonProjectFile(string fileFullPath)
		{
			return fileFullPath.Contains(CommonProjectName);
		}
	}

}
