﻿using System.Collections.Generic;
using System.IO.Abstractions;
using System.Net.Http;

namespace GitLabApi
{
	public class GitLab
	{
		private HttpClient _httpClient;
		private FileSystem _fileSystem;
		private string _privateToken;

		private const int LAST_PUSHED_COMMIT_INDEX=0;

		public GitLab(HttpClient httpClient)
		{
			_httpClient = httpClient;
			_fileSystem = new FileSystem();
			_privateToken = _fileSystem.File.ReadAllText("Properties/privateToken.tok");
		}

		public List<string> GetLastPushedCommitsIds(string oldCommitId)
		{
			var commitsArray = GetAllMasterCommits();

			var lastPushedCommitsIds = GetMasterPushedCommitsSinceOldCommitId(oldCommitId, commitsArray);

			return lastPushedCommitsIds;
		}

		private static List<string> GetMasterPushedCommitsSinceOldCommitId(string oldCommitId, Commit[] commitsArray)
		{
			var commitIds = new List<string>();
			foreach (var commit in commitsArray)
			{
				if (commit.id == oldCommitId)
					break;
				commitIds.Add(commit.id);
			}
			return commitIds;
		}

		public List<string> GetCommitedFiles(List<string> commitIds)
		{
			var commitedFiles = new List<string>();
			foreach (var commitId in commitIds)
			{
				var response = _httpClient.GetAsync($"https://gitlab.com/api/v4/projects/7106488/repository/commits/{commitId}/diff?private_token={_privateToken}").Result;
				var differencesArray = response.Content.ReadAsAsync<CommitDifferences[]>().Result;
				foreach (var commitDifference in differencesArray)
				{
					commitedFiles.Add(commitDifference.old_path);
					commitedFiles.Add(commitDifference.new_path);
				}
			}
			return commitedFiles;
		}

		public string GetMasterLastPushedCommitId()
		{
			return GetAllMasterCommits()[LAST_PUSHED_COMMIT_INDEX].id;
		}

		private Commit[] GetAllMasterCommits()
		{
			var response = _httpClient.GetAsync($"https://gitlab.com/api/v4/projects/7106488/repository/commits?private_token={_privateToken}").Result;
			var commitsArray = response.Content.ReadAsAsync<Commit[]>().Result;
			return commitsArray;
		}
	}
}
