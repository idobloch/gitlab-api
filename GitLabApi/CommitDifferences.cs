﻿namespace GitLabApi
{
	public class CommitDifferences
	{
		public string diff { get; set; }
		public string new_path { get; set; }
		public string old_path { get; set; }
		public string a_mode { get; set; }
		public string b_mode { get; set; }
		public bool new_file { get; set; }
		public bool renamed_file { get; set; }
		public bool deleted_file { get; set; }


	}
}
